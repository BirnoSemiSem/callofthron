﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Musicscript : MonoBehaviour
{
    // Start is called before the first frame update


    public AudioSource AudioFight1;
    public AudioSource AudioFight2;
    public AudioSource AudioCommentator;
    public AudioClip[] MusicClips;
    public AudioClip[] CommentatorClips;

    private float TimerS = GameScript.TimerSecond;
    private float TimerM = GameScript.TimerMinute;
    private float TimerMusicFade;
    public float MusicFadeSecond;

    private bool FadeSAppearanceStart = false;
    private bool FadeSDecayEnd = false;

    private bool FadeS1 = false;
    private bool FadeS2 = false;

    IEnumerator fadeSoundAppearanceStart;
    IEnumerator fadeSoundDecayEnd;

    IEnumerator[] fadeSound1 = new IEnumerator[3];
    IEnumerator[] fadeSound2 = new IEnumerator[3];
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (GameScript.isGameStarted)
        {
            TimerS = GameScript.TimerSecond;
            TimerM = GameScript.TimerMinute;

            //if (!AudioFight1.isPlaying && !AudioFight2.isPlaying)
            //{
            //    switch (CurrentMussic)
            //    {
            //        case "C":
            //        {
            //            if(FadeS1)
            //                AudioFight1.PlayOneShot(Clips[0]);
            //            else
            //            if(FadeS2)
            //                AudioFight2.PlayOneShot(Clips[0]);
            //            break;
            //        }
            //        case "B":
            //        {
            //            if (FadeS1)
            //                AudioFight1.PlayOneShot(Clips[1]);
            //            else
            //            if (FadeS2)
            //                AudioFight2.PlayOneShot(Clips[1]);
            //            break;
            //        }
            //        case "A":
            //        {
            //            if (FadeS1)
            //                AudioFight1.PlayOneShot(Clips[2]);
            //            else
            //            if (FadeS2)
            //                AudioFight2.PlayOneShot(Clips[2]);
            //            break;
            //        }
            //    }
            //}

            if (RankPlayerBatl.RankPlayer == 1 && !FadeS1)
            {
                AudioFight1.volume = 0.0f;
                fadeSoundAppearanceStart = Fade(AudioFight1, 8.5f, 0.8f);
                StartCoroutine(fadeSoundAppearanceStart);

                TimerMusicFade = TimerS + MusicFadeSecond;

                FadeSAppearanceStart = true;
                FadeS1 = true;
                FadeS2 = false;

                AudioCommentator.clip = CommentatorClips[0];
                AudioCommentator.Play();

                AudioFight1.clip = MusicClips[RankPlayerBatl.RankPlayer-1];
                AudioFight1.Play();
                AudioFight1.loop = true;

                fadeSound1[1] = Fade(AudioFight1, MusicFadeSecond, 0.0f);
                fadeSound2[1] = Fade(AudioFight2, MusicFadeSecond, 0.8f);
            }
            else if (RankPlayerBatl.RankPlayer == 2 && !FadeS2)
            {
                AudioFight2.volume = 0.0f;
                StartCoroutine(fadeSound1[1]);
                StartCoroutine(fadeSound2[1]);

                TimerMusicFade = TimerS + MusicFadeSecond;

                FadeS2 = true;
                FadeS1 = false;

                AudioCommentator.clip = CommentatorClips[1];
                AudioCommentator.Play();

                AudioFight2.clip = MusicClips[RankPlayerBatl.RankPlayer - 1];
                AudioFight2.Play();
                AudioFight2.loop = true;

                fadeSound2[2] = Fade(AudioFight2, MusicFadeSecond, 0.0f);
                fadeSound1[2] = Fade(AudioFight1, MusicFadeSecond, 0.8f);
            }
            else if (RankPlayerBatl.RankPlayer == 3 && !FadeS1)
            {
                AudioFight1.volume = 0.0f;
                StartCoroutine(fadeSound2[2]);
                StartCoroutine(fadeSound1[2]);

                TimerMusicFade = TimerS + MusicFadeSecond;

                FadeS1 = true;
                FadeS2 = false;

                AudioCommentator.clip = CommentatorClips[2];
                AudioCommentator.Play();

                AudioFight1.clip = MusicClips[RankPlayerBatl.RankPlayer - 1];
                AudioFight1.Play();
                AudioFight1.loop = true;

                fadeSound1[1] = Fade(AudioFight1, MusicFadeSecond, 0.0f);
                fadeSound2[1] = Fade(AudioFight2, MusicFadeSecond, 0.8f);
            }
            else if (RankPlayerBatl.RankPlayer == 4 && !FadeS2)
            {
                AudioFight2.volume = 0.0f;
                StartCoroutine(fadeSound1[1]);
                StartCoroutine(fadeSound2[1]);

                TimerMusicFade = TimerS + MusicFadeSecond;

                FadeS2 = true;
                FadeS1 = false;

                AudioCommentator.clip = CommentatorClips[3];
                AudioCommentator.Play();

                AudioFight2.clip = MusicClips[RankPlayerBatl.RankPlayer - 1];
                AudioFight2.Play();
                AudioFight2.loop = true;

                fadeSound2[2] = Fade(AudioFight2, MusicFadeSecond, 0.0f);
                fadeSound1[2] = Fade(AudioFight1, MusicFadeSecond, 0.8f);
            }
            else if (RankPlayerBatl.RankPlayer == 5 && !FadeS1)
            {
                AudioFight1.volume = 0.0f;
                StartCoroutine(fadeSound2[2]);
                StartCoroutine(fadeSound1[2]);

                TimerMusicFade = TimerS + MusicFadeSecond;

                FadeS1 = true;
                FadeS2 = false;

                AudioCommentator.clip = CommentatorClips[4];
                AudioCommentator.Play();

                AudioFight1.clip = MusicClips[RankPlayerBatl.RankPlayer - 1];
                AudioFight1.Play();
                AudioFight1.loop = true;

                fadeSound1[1] = Fade(AudioFight1, MusicFadeSecond, 0.0f);
                fadeSound2[1] = Fade(AudioFight2, MusicFadeSecond, 0.8f);
            }

            if (TimerS > TimerMusicFade)
            {
                if(FadeSAppearanceStart)
                {
                    StopCoroutine(fadeSoundAppearanceStart);
                    FadeSAppearanceStart = false;
                }

                if(FadeS1)
                {
                    StopCoroutine(fadeSound1[1]);
                    StopCoroutine(fadeSound2[1]);
                    AudioFight2.Stop();
                }

                if(FadeS2)
                {
                    StopCoroutine(fadeSound1[2]);
                    StopCoroutine(fadeSound2[2]);
                    AudioFight1.Stop();
                }
            }
        }
    }


    public static IEnumerator Fade(AudioSource audioSource, float duration, float targetVolume)
    {
        float currentTime = 0;
        float start = audioSource.volume;

        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            audioSource.volume = Mathf.Lerp(start, targetVolume, currentTime / duration);
            yield return null;
        }
        yield break;
    }
}
