﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSound : MonoBehaviour
{
    // Start is called before the first frame update
    private Animator anim;

    public AudioSource EffectSounds;
    public AudioClip[] ClipsRun;

    public AudioClip[] ClipsDaggerAttackPunch;
    public AudioClip[] ClipsDaggerAttackSwing;
    public AudioClip[] ClipsKatanaAttackPunch;
    public AudioClip[] ClipsKatanaAttackSwing;
    public AudioClip[] ClipsSwithWeapon;

    public AudioClip ClipJump;
    public AudioClip ClipDown;

    //Sound Run
    private float TimeSound;
    private float CurrentTime = 0.0f;

    private bool IsHitAttack = false;

    static public bool IsSoundRun = false;
    static public bool IsSoundAttack = false;
    static public bool IsSoundJump = false;
    static public bool IsSoundDown = false;

    void Start()
    {
        anim = GetComponent<Animator>();
        TimeSound = Time.deltaTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameScript.isGame && !AnimatorPlayer.TackDamagePain)
        {
            CurrentTime += Time.deltaTime;

            if (IsSoundRun)
            {
                if (TimeSound < CurrentTime)
                {
                    TimeSound = CurrentTime + 0.3f;
                    EffectSounds.PlayOneShot(ClipsRun[Random.Range(0, ClipsRun.Length)]);
                    IsSoundRun = false;
                }
            }

            if (IsSoundAttack)
            {
                //Attack1.PlayOneShot(ClipsAttack1[Random.Range(0, ClipsAttack1.Length)]);
                CancelInvoke("AttackSound");
                Invoke("AttackSound", 0.1f);
                IsSoundAttack = false;
            }

            if (IsSoundJump)
            {
                EffectSounds.PlayOneShot(ClipJump);
                IsSoundJump = false;
            }

            if (IsSoundDown)
            {
                EffectSounds.PlayOneShot(ClipDown);
                IsSoundDown = false;
            }

            if (Input.GetKeyDown(KeyCode.X) && (anim.GetCurrentAnimatorStateInfo(0).IsName("IdleDagger") || anim.GetCurrentAnimatorStateInfo(0).IsName("IdleKatana")) && !AnimatorPlayer.TackDamagePain)
            {
                if(PlayerControl.TypeWeapon)
                    EffectSounds.PlayOneShot(ClipsSwithWeapon[0]);
                else
                if(!PlayerControl.TypeWeapon)
                    EffectSounds.PlayOneShot(ClipsSwithWeapon[1]);
            }
        }
    }

    void AttackSound()
    {
        if(IsHitAttack)
        {
            if(PlayerControl.TypeWeapon)
                EffectSounds.PlayOneShot(ClipsKatanaAttackPunch[Random.Range(0, ClipsKatanaAttackPunch.Length)]);
            else
                EffectSounds.PlayOneShot(ClipsDaggerAttackPunch[Random.Range(0, ClipsDaggerAttackPunch.Length)]);
        }
        else
        {
            if (PlayerControl.TypeWeapon)
                EffectSounds.PlayOneShot(ClipsKatanaAttackSwing[Random.Range(0, ClipsKatanaAttackSwing.Length)]);
            else
                EffectSounds.PlayOneShot(ClipsDaggerAttackSwing[Random.Range(0, ClipsDaggerAttackSwing.Length)]);
        }
        IsHitAttack = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(AnimatorPlayer.IsAttacking)
        {
            if(collision.tag == "Enemy")
            {
                IsHitAttack = true;
            }
        }
    }
}
