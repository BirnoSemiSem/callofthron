﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptIntro : MonoBehaviour
{
    // Start is called before the first frame update


    
    public GameObject Intro;
    public GameObject MainMenu;

    void Start()
    {
        Invoke("IntroEnd", 4.0f);
    }

    void IntroEnd()
    {
        Intro.SetActive(false);
        MainMenu.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
