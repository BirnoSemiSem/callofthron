﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class StreamVideo : MonoBehaviour {
     public RawImage[] rawImage;
     public VideoPlayer videoPlayer;
     //public AudioSource audioSource;
  // Use this for initialization
  void Start () {
          StartCoroutine(PlayVideo());
  }
  IEnumerator PlayVideo()
     {
          videoPlayer.Prepare();
          WaitForSeconds waitForSeconds = new WaitForSeconds(1);
          while (!videoPlayer.isPrepared)
          {
               yield return waitForSeconds;
               break;
          }
          for(int i = 0; i < rawImage.Length; i++)
            rawImage[i].texture = videoPlayer.texture;
          videoPlayer.Play();
          //audioSource.Play();
     }
}