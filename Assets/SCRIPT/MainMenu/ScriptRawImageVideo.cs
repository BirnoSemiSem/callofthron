﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptRawImageVideo : MonoBehaviour
{
    // Start is called before the first frame update
    public RectTransform[] RawImage;
    public RectTransform[] Canvas;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < RawImage.Length; i++)
            RawImage[i].sizeDelta = new Vector2(Canvas[i].sizeDelta.x, Canvas[i].sizeDelta.y);
    }
}
