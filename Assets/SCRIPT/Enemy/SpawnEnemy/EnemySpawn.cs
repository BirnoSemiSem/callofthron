﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnScript : MonoBehaviour
{
    // Start is called before the first frame update
    public int Enemys = 1;
    public int CountEnemys = 0;
    public GameObject[] EnemysObject;
    public GameObject[] SpawnPoint;
    public int wave = 0;
    public int[] SecondSpawn = {5, 20, 40, 80, 120, 160, 180, 240, 300 };
    private float Second;
    void Start()
    {
        Second = 0;
        wave = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(GameScript.isGame && GameScript.isGameStarted)
        {
            Second += 1 * Time.deltaTime;
            if (Second >= SecondSpawn[wave])
            {
                Spawn();
                wave++;
            }
        }
    }

    void Spawn()
    {
        if (GameScript.isGame && GameScript.isGameStarted)
        {
            if (CountEnemys < Enemys)
            {
                GameObject Enemy = Instantiate(EnemysObject[Random.Range(0, EnemysObject.Length)]) as GameObject;
                Enemy.transform.position = SpawnPoint[Random.Range(0, SpawnPoint.Length)].transform.position;
                CountEnemys++;
                Invoke("Spawn", 1.2f);
            }
            else if(CountEnemys == Enemys)
            {
                Enemys += 1;
                CountEnemys = 0;
            }
        }
    }
}
