﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    public enum Enemy
    {
        Wolf,
        Skilet
    };

    public Enemy enemy;

    public float speed;
    public int possitonOfPatrol;
    public Transform point;
    public bool moving;

    bool chill = false;
    bool angry = false;

    bool facingRight;
    static public bool IsAttackingEnemy = false;

    Transform player;
    public float stoopingDistanse;

    private Animator anim;
    private Rigidbody2D rb;
    private BoxCollider2D HitBox;

    public AudioSource Attack1;
    public AudioClip[] ClipsAttack1;

    public GameObject attack1HitBox;

    private int health = 10000;
    public float rayDistance = 2;
    public float timeattack = 0.5f;
    public float CurrentTime;
    private float TimeSound;

    private Material matBlink;
    private Material matDefault;
    private SpriteRenderer SpriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        HitBox = GetComponent<BoxCollider2D>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        SpriteRenderer = GetComponent<SpriteRenderer>();
        matBlink = Resources.Load("EnemyBlink", typeof(Material)) as Material;
        matDefault = SpriteRenderer.material;
        Physics2D.queriesStartInColliders = false;
        attack1HitBox.SetActive(false);
        TimeSound = Time.deltaTime;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("DamageKnifeHeroForEnemys"))
        {
            if (RankPlayerBatl.RankPlayer == 0)
                RankPlayerBatl.ProgressBarHealth = 1.0f;

            //switch (RankPlayerBatl.RankPlayer)
            //{
            //    case 1: RankPlayerBatl.ProgressBarHealth += Random.Range(0.03f, 0.05f); break;
            //    case 2: RankPlayerBatl.ProgressBarHealth += Random.Range(0.03f, 0.04f); break;
            //    case 3: RankPlayerBatl.ProgressBarHealth += Random.Range(0.02f, 0.03f); break;
            //    case 4: RankPlayerBatl.ProgressBarHealth += Random.Range(0.02f, 0.025f); break;
            //    case 5: RankPlayerBatl.ProgressBarHealth += Random.Range(0.015f, 0.03f); break;
            //}

            RankPlayerBatl.ProgressBarHealth += Random.Range(0.03f, 0.05f);

            health -= Random.Range(8, 15);

            SpriteRenderer.material = matBlink;

            if (health <= 0)
            {
                Kill();
            }
            else
            {
                Invoke("ResetMatedial", 0.2f);
            }
        }
    }

    void ResetMatedial()
    {
        SpriteRenderer.material = matDefault;
    }

    void Kill()
    {
        //Destroy(gameObject);
        IsAttackingEnemy = false;
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameScript.isGame)
        {
            CurrentTime += Time.deltaTime;

            if (!IsAttackingEnemy)
            {
                if (Vector2.Distance(transform.position, point.position) < possitonOfPatrol && !angry)
                {
                    chill = true;
                }

                if (Vector2.Distance(transform.position, player.position) < stoopingDistanse)
                {
                    angry = true;
                    chill = false;
                }
                else
                    angry = false;
            }

            if (chill)
            {
                Chill();
                anim.SetBool("isRunning", true);
            }
            else if (angry && !IsAttackingEnemy)
            {
                anim.SetBool("isRunning", true);
                Angry();
            }

            if (IsAttackingEnemy)
            {
                if(enemy == Enemy.Wolf)
                    rb.velocity = new Vector2((transform.localScale.x * -1) * 40.0f, rb.velocity.y);
            }
        }
    }

    void Chill()
    {
        if(transform.position.x > point.position.x + possitonOfPatrol)
        {
            if (moving)
            {
                Vector3 Scaler = transform.localScale;
                Scaler.x *= -1;
                transform.localScale = Scaler;
            }
            moving = false;
        }
        else if(transform.position.x < point.position.x - possitonOfPatrol)
        {
            if (!moving)
            {
                Vector3 Scaler = transform.localScale;
                Scaler.x *= -1;
                transform.localScale = Scaler;
            }
            moving = true;
        }

        if(moving)
        {
            transform.position = new Vector3(transform.position.x + speed * Time.deltaTime, transform.position.y, transform.position.z);
        }
        else
        {
            transform.position = new Vector3(transform.position.x - speed * Time.deltaTime, transform.position.y, transform.position.z);
        }
    }

    void Angry()
    {
        if (transform.position.x > player.position.x+250)
        {
            if (moving)
            {
                Vector3 Scaler = transform.localScale;
                Scaler.x *= -1;
                transform.localScale = Scaler;
            }
            moving = false;
        }
        else if (transform.position.x < player.position.x-250)
        {
            if (!moving)
            {
                Vector3 Scaler = transform.localScale;
                Scaler.x *= -1;
                transform.localScale = Scaler;
            }
            moving = true;
        }

        if (moving)
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.localScale.x * Vector2.left, rayDistance * rayDistance + (rayDistance * rayDistance) * 0.5f);

            //Debug.Log("HIT: " + hit.collider + " Moving: " + moving);

            if (hit.collider != null)
            {
                if (TimeSound < CurrentTime)
                {
                    TimeSound = CurrentTime + 0.6f;
                    Invoke("AttackEnemy", timeattack);
                }
            }
            if(Vector2.Distance(transform.position, player.position) > 250)
                transform.position = new Vector3(transform.position.x + speed * Time.deltaTime, transform.position.y, transform.position.z);
        }
        else if (!moving)
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.localScale.x * Vector2.left, rayDistance* rayDistance+ (rayDistance * rayDistance)*0.5f);

            //Debug.Log("HIT: " + hit.collider + " Moving: " + moving);

            if (hit.collider != null)
            {
                if (TimeSound < CurrentTime)
                {
                    TimeSound = CurrentTime + 0.6f;
                    Invoke("AttackEnemy", timeattack);
                }
            }

            if (Vector2.Distance(transform.position, player.position) > 250)
                transform.position = new Vector3(transform.position.x - speed * Time.deltaTime, transform.position.y, transform.position.z);
        }
    }

    private void OnDrawGizmos()
    {
        //Gizmos.color = Color.red;
        //Gizmos.DrawLine(transform.position, transform.position + transform.localScale.x * Vector3.right * rayDistance);

        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + transform.localScale.x * Vector3.left * rayDistance);
    }

    void AttackEnemy()
    {
        IsAttackingEnemy = true;

        anim.Play("Attack");
        Attack1.PlayOneShot(ClipsAttack1[Random.Range(0, ClipsAttack1.Length)]);

        if(enemy == Enemy.Wolf)
            rb.velocity = new Vector2((transform.localScale.x * -1) * 40.0f, rb.velocity.y);

        StartCoroutine(DoAttack());
    }

    IEnumerator DoAttack()
    {
        attack1HitBox.SetActive(true);
        HitBox.offset = new Vector2(HitBox.offset.x+0.5f, HitBox.offset.y + 0.5f);
        yield return new WaitForSeconds(0.5f);
        attack1HitBox.SetActive(false);
        IsAttackingEnemy = false;
        yield return new WaitForSeconds(0.5f);
        HitBox.offset = new Vector2(HitBox.offset.x - 0.5f, HitBox.offset.y - 0.5f);
        rb.velocity = new Vector2(0.0f, rb.velocity.y);
    }
}
