﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIinGame : MonoBehaviour
{
    public Image WeaponType;
    public Sprite[] WepoanTypeImagesSource;
    public GameObject ObjectPainBloodEffect;

    public static Animation PainBloodEffect;

    // Start is called before the first frame update
    void Start()
    {
        PainBloodEffect = ObjectPainBloodEffect.GetComponent<Animation>();
    }

    // Update is called once per frame
    void Update()
    {
        if(WeaponType != null)
            WeaponType.sprite = WepoanTypeImagesSource[PlayerControl.TypeWeapon ? 1 : 0];
    }

    static public void BloodEffect()
    {
        PainBloodEffect.Play();
    }
}
