﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Visualizer : MonoBehaviour
{
    public float minHeight = 15.0f;
    public float maxHeight = 425.0f;
    public float updateSentivity = 10.0f;
    [Range(64, 8192)]
    public int visualizerSimples = 64;
    float[] spectrumData;

    public GameObject[] visualizerObjects;
    public AudioSource[] audioSource = new AudioSource[2];

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (RankPlayerBatl.RankPlayer > 0)
        {
            if (audioSource[0].isPlaying && audioSource[1].isPlaying)
                spectrumData = Random.Range(0, 10) > 5 ? audioSource[0].GetSpectrumData(visualizerSimples, Random.Range(0, 1), FFTWindow.Rectangular) : audioSource[1].GetSpectrumData(visualizerSimples, Random.Range(0, 1), FFTWindow.Rectangular);
            else if (audioSource[0].isPlaying)
                spectrumData = audioSource[0].GetSpectrumData(visualizerSimples, Random.Range(0, 1), FFTWindow.Rectangular);
            else if (audioSource[1].isPlaying)
                spectrumData = audioSource[1].GetSpectrumData(visualizerSimples, Random.Range(0, 1), FFTWindow.Rectangular);

            for (int i = 0; i < visualizerObjects.Length; i++)
            {
                Vector2 newSize = visualizerObjects[i].GetComponent<RectTransform>().rect.size;

                newSize.y = Mathf.Clamp(Mathf.Lerp(newSize.y, minHeight + (spectrumData[i] * (maxHeight - minHeight) * 5.0f), updateSentivity * 0.5f), minHeight, maxHeight);
                visualizerObjects[i].GetComponent<RectTransform>().sizeDelta = newSize;
            }
        }
    }
}
