﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthPlayer : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject[] BlockHealth;
    public GameObject[] GreenBlock;
    public GameObject[] BlueBlock;
    public GameObject[] YellowBlock;
    public GameObject[] RedBlock;

    private Animation[] BigBlockImage = new Animation[4];
    private Animation[] SmallBlockImage = new Animation[20];


    private bool[] AnimationBlockImage = new bool[20];
    private bool[] BlockImageHealth = new bool[4];

    private int HealthBlock;

    void Start()
    {
        HealthBlock = GreenBlock.Length + BlueBlock.Length + YellowBlock.Length + RedBlock.Length;

        for (int i = 0; i < HealthBlock; i++)
        {
            if (i < 5)
            {
                SmallBlockImage[i] = RedBlock[i].GetComponent<Animation>();

                if(i < 4)
                    BigBlockImage[i] = BlockHealth[i].GetComponent<Animation>();
            }
            else if (i < 10)
                SmallBlockImage[i] = YellowBlock[i - 5].GetComponent<Animation>();
            else if (i < 15)
                SmallBlockImage[i] = BlueBlock[i - 10].GetComponent<Animation>();
            else
                SmallBlockImage[i] = GreenBlock[i - 15].GetComponent<Animation>();
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (AnimatorPlayer.TackDamagePain)
        {
            for (int i = PlayerControl.HealthPlayer > 0 ? PlayerControl.HealthPlayer / 5 : 0; i < 20; i++)
            {
                if (!AnimationBlockImage[i])
                {
                    AnimationBlockImage[i] = true;
                    SmallBlockImage[i].Play();
                }
            }

            if (PlayerControl.HealthPlayer <= 75 && !BlockImageHealth[0])
            {
                BigBlockImage[0].Play();
                BlockImageHealth[0] = true;
            }
            else if (PlayerControl.HealthPlayer <= 50 && !BlockImageHealth[1])
            {
                BigBlockImage[1].Play();
                BlockImageHealth[1] = true;
            }
            else if(PlayerControl.HealthPlayer <= 25 && !BlockImageHealth[2])
            {
                BigBlockImage[2].Play();
                BlockImageHealth[2] = true;
            }
            else if(PlayerControl.HealthPlayer <= 0 && !BlockImageHealth[3])
            {
                BigBlockImage[3].Play();
                BlockImageHealth[3] = true;
            }
        }
    }
}
