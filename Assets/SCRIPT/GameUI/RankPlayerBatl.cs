﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankPlayerBatl : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject Rankbatl;
    public GameObject TextRank;
    public GameObject ProgressBarRank;

    private Image ImRank;
    private Image ImText;
    private Image ImBar;

    public Sprite[] SpRank = new Sprite[5];
    public Sprite[] SpText = new Sprite[5];
    public Sprite[] SpBar = new Sprite[5];

    private Animation Rank;

    public static float ProgressBarHealth = 0.0f;
    public static int RankPlayer = 0;

    void Start()
    {
        ImRank = Rankbatl.GetComponent<Image>();
        ImText = TextRank.GetComponent<Image>();
        ImBar = ProgressBarRank.GetComponent<Image>();

        Rank = Rankbatl.GetComponent<Animation>();
    }

    // Update is called once per frame
    void Update()
    {
        ImBar.fillAmount = ProgressBarHealth;

        if (ProgressBarHealth >= 1f && RankPlayer < 5)
        {
            RankPlayer++;
            RankLogic();
        }

        if (RankPlayer == 0)
        {
            ProgressBarHealth = 0f;

            Rankbatl.SetActive(false);
            TextRank.SetActive(false);
            ProgressBarRank.SetActive(false);
        }
    }

    void RankLogic()
    {
        switch (RankPlayer)
        {
            case 1:
            {
                ProgressBarHealth = 0.3f;

                Rankbatl.SetActive(true);
                TextRank.SetActive(true);
                ProgressBarRank.SetActive(true);

                ImRank.sprite = SpRank[RankPlayer - 1];
                ImText.sprite = SpText[RankPlayer - 1];
                ImBar.sprite = SpBar[RankPlayer - 1];

                Rank.Play();
                break;
            }
            case 2:
            {
                ProgressBarHealth = 0.3f;

                ImRank.sprite = SpRank[RankPlayer - 1];
                ImText.sprite = SpText[RankPlayer - 1];
                ImBar.sprite = SpBar[RankPlayer - 1];

                Rank.Play();
                break;
            }
            case 3:
            {
                ProgressBarHealth = 0.25f;

                ImRank.sprite = SpRank[RankPlayer - 1];
                ImText.sprite = SpText[RankPlayer - 1];
                ImBar.sprite = SpBar[RankPlayer - 1];

                Rank.Play();
                break;
            }
            case 4:
            {
                ProgressBarHealth = 0.25f;

                ImRank.sprite = SpRank[RankPlayer - 1];
                ImText.sprite = SpText[RankPlayer - 1];
                ImBar.sprite = SpBar[RankPlayer - 1];

                Rank.Play();
                break;
            }
            case 5:
            {
                ProgressBarHealth = 0.2f;

                ImRank.sprite = SpRank[RankPlayer - 1];
                ImText.sprite = SpText[RankPlayer - 1];
                ImBar.sprite = SpBar[RankPlayer - 1];

                Rank.Play();
                break;
            }
        }
    }
}
