﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugForCheats : MonoBehaviour
{
    // Start is called before the first frame update
    static public bool isGodMod;
    static public bool isNoPain;
    static public bool isTimeGame;

    public bool GodMod = false;
    public bool Nopain = false;
    public bool TimeGame = false;

    public float TimeGameValue = 1.0f;
    void Start()
    {
        isGodMod = GodMod;
        isNoPain = Nopain;
        isTimeGame = TimeGame;
    }

    // Update is called once per frame
    void Update()
    {
        isGodMod = GodMod;
        isNoPain = Nopain;
        isTimeGame = TimeGame;

        if(isTimeGame)
            Time.timeScale = TimeGameValue;
    }
}
