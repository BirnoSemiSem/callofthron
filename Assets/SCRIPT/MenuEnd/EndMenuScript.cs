﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndMenuScript : MonoBehaviour
{
    public GameObject ButtonGameRestat;
    public GameObject ButtonGameSettings;
    public GameObject ButtonGameExitMenu;
    public GameObject ButtonGameExit;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!GameScript.isGame)
        {
            Cursor.visible = true;
            Time.timeScale = 0f;
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene("TestLevel");
    }

    public void ExitMenu()
    {
        SceneManager.LoadScene("MAINMENU");
    }

    public void Exit()
    {
        Application.Quit();
    }
}
