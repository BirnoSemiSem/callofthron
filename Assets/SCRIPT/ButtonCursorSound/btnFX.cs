﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class btnFX : MonoBehaviour {
	
	public AudioSource myFx;
	public AudioClip hoverFx;
	public AudioClip clickFx;
    public Texture2D cursorTextureAim;
    public Texture2D cursorTextureClose;
    private CursorMode cursorMode = CursorMode.Auto;
    private Vector2 hotSpot = Vector2.zero;


    public void HoverSound()
	{
		myFx.PlayOneShot (hoverFx);
        Cursor.SetCursor(cursorTextureAim, hotSpot, cursorMode);
    }
	public void ClickSound()
	{
		myFx.PlayOneShot (clickFx);
	}

    public void Close()
    {
        Cursor.SetCursor(cursorTextureClose, hotSpot, cursorMode);
    }
}