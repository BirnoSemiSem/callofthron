﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameScript : MonoBehaviour
{
    // Start is called before the first frame update

    static public float TimerSecond = 5;
    static public float TimerMinute = 0;
    static public bool isGameStarted = false;
    static public bool isGame = true;
    //public Text textboxTimer;

    public GameObject GameEndMenu;
    public GameObject CanvasTimer;

    void Start()
    {
        TimerSecond = 5;
        TimerMinute = 0;
        isGameStarted = false;
        //textboxTimer.text = $"{TimerMinute.ToString()}:{TimerSecond.ToString("F2")} Traning...";
        isGame = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(!isGameStarted && isGame)
        {
            if (TimerMinute > 0 && TimerSecond <= 0)
            {
                TimerMinute--;
                TimerSecond = 59;
                //textboxTimer.text = $"{TimerMinute.ToString()}:{TimerSecond.ToString("F2")} \n Traning...";
            }
            else if (TimerMinute <= 0 && TimerSecond <= 0)
            {
                isGameStarted = true;
                //textboxTimer.text = $"{TimerMinute.ToString()}:{TimerSecond.ToString("F2")} \n START!!!";
                TimerSecond = 0;
            }
            else if(TimerSecond > 0)
            {
                TimerSecond -= 1 * Time.deltaTime;
                //textboxTimer.text = $"{TimerMinute.ToString()}:{TimerSecond.ToString("F2")} \n Traning...";
            }
        }
        else if(isGameStarted && isGame)
        {
            if (TimerSecond >= 59)
            {
                TimerMinute++;
                TimerSecond = 0;
                //textboxTimer.text = $"{TimerMinute.ToString()}:{TimerSecond.ToString("F2")}";
            }
            else if(TimerMinute <= 0 && TimerSecond <= 10)
            {
                TimerSecond += 1 * Time.deltaTime;
                //textboxTimer.text = $"{TimerMinute.ToString()}:{TimerSecond.ToString("F2")} \n START!!!";
            }
            else
            {
                TimerSecond += 1 * Time.deltaTime;
                //textboxTimer.text = $"{TimerMinute.ToString()}:{TimerSecond.ToString("F2")}";
            }
        }
        else if(!isGame)
        {
            CanvasTimer.SetActive(false);
            GameEndMenu.SetActive(true);
        }
    }
}