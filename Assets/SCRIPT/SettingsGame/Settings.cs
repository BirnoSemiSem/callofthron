﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    // Start is called before the first frame update
    public Dropdown DropdownVideoResolution;
    public Button SaveButton;
    public InputField Nick;

    public GameObject HelloCanvas;
    public Button ButtonSaveHello;

    private string VideoResolution;
    private string FileSettings = Environment.CurrentDirectory + @"\config.settings";
    void Start()
    {
        if (!File.Exists(FileSettings))
            FileSettingsCreate();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ClickSaveButton()
    {
        if(File.Exists(FileSettings))
        {
            XDocument xdoc = XDocument.Load(FileSettings);
            XElement root = xdoc.Element("Settings");

            //VideoResolution = DropdownVideoResolution.options[DropdownVideoResolution.value].text;

            foreach (XElement xe in root.Elements("User").ToList())
            {
                xe.Element("NamePlayer").Value = Nick.text;
            }
            xdoc.Save(FileSettings);

            //CameraViews();
        }
    }

    private void CameraViews()
    {
        switch(VideoResolution)
        {
            case "640x480":
            {
                //Camera. = 640;
                //Screen.width = 480;
                break;
            }
        }
    }

    private void FileSettingsCreate()
    {
        XDocument xdoc = new XDocument();
        XElement ObjSettings = new XElement("Settings");

        XElement User = new XElement("User");

        XElement ObjNamePlayer = new XElement("NamePlayer", "Player");
        //XElement ObjVideoResolution = new XElement("VideoResolution", "1024x768");

        User.Add(ObjNamePlayer);
        //User.Add(ObjVideoResolution);
        ObjSettings.Add(User);

        xdoc.Add(ObjSettings);
        xdoc.Save(FileSettings);
    }

    public void SaveHelloButton()
    {
        ClickSaveButton();
        HelloCanvas.SetActive(false);
    }
}
