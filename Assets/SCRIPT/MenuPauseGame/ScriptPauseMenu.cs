﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScriptPauseMenu : MonoBehaviour
{
    public static bool isGamePause = false;
    public GameObject pauseMenuUI;

    public GameObject ButtonLoadingLastCheckpoint;
    public GameObject ButtonGameSettings;
    public GameObject ButtonGameExitMenu;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameScript.isGame)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (isGamePause)
                {
                    Cursor.visible = false;
                    Resume();
                }
                else
                {
                    //btnFX.native_Close();
                    Pause();
                }
            }
        }
    }

    public void Resume()
    {
        Cursor.visible = false;
        pauseMenuUI.SetActive(false);
        //GameSettings.SetActive(false);

        Time.timeScale = 1f;
        isGamePause = false;
    }

    public void Pause()
    {
        Cursor.visible = true;
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        isGamePause = true;
    }

    public void Settings()
    {
        //GameSettings.SetActive(true);
    }

    public void ExitMenu()
    {
        SceneManager.LoadScene("MAINMENU");
    }
}
