﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject MenuWelcom;               // 0
    public GameObject MainMenu;                 // 1
    public GameObject MenuSettings;             // 2
    public GameObject MenuLicense;              // 3

    private GameObject[] Menu;

    public AudioSource audio;
    public AudioClip audioClip;

    public GameObject EffectDown;
    private Animation anim;

    private Coroutine MenuCorotine = null;

    void Start()
    {
        Cursor.visible = true;
        anim = EffectDown.GetComponent<Animation>();
        Menu = new GameObject[] { MenuWelcom, MainMenu, MenuSettings, MenuLicense };
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKey && (MenuWelcom.activeInHierarchy || MenuLicense.activeInHierarchy))
        {
            if (!MenuLicense.activeInHierarchy && !audio.isPlaying)
                audio.PlayOneShot(audioClip);

            MainMenuControl();
        }
    }

    public void MainMenuControl()
    {
        if (MenuCorotine == null)
        {
            anim.Play("EffectDimming");
            MenuCorotine = StartCoroutine(MenuControl(1));
        }
    }

    public void SettingsControl()
    {
        if (MenuCorotine == null)
        {
            anim.Play("EffectDimming");
            MenuCorotine = StartCoroutine(MenuControl(2));
        }
    }

    public void LicenseControl()
    {
        if (MenuCorotine == null)
        {
            anim.Play("EffectDimming");
            MenuCorotine = StartCoroutine(MenuControl(3));
        }
    }

    IEnumerator MenuControl(int MenuIsActive)
    {
        yield return new WaitForSeconds(anim.clip.length / 2);

        for(int i = 0; i < Menu.Length; i++)
        {
            if (i == MenuIsActive)
                Menu[i].SetActive(true);
            else
                Menu[i].SetActive(false);
        }

        yield return new WaitForSeconds(anim.clip.length / 2);
        MenuCorotine = null;
    }

    public void GameNew()
    {
        SceneManager.LoadScene("TestLevel");
    }

    public void ExitControlControl()
    {
        Application.Quit();
    }
}