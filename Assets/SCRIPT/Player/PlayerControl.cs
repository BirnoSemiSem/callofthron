﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour
{
    // Start is called before the first frame update

    public float speed = 30f;
    public float ConstNormalAnimationSpeed = 30f;

    public float JumpForce;
    public float CheckRadious;

    static public bool TypeWeapon = false;

    //bool Buttons
    public static bool b_Left = false;
    public static bool b_Right = false;

    private Animator Anim;
    private float TimerRun;

    private float moveX;
    private float moveY;
    private bool facingRight = true;
    static public bool IsGrounded;
    static public bool IsOverEnemy;

    public Transform GroundCheck;
    public LayerMask WhatIsGround;
    public LayerMask WhatIsEnemy;

    private Rigidbody2D rb;

    static public int HealthPlayer = 100;
    static public int Damage;
    private int RandomEnemyOver;

    public GameObject attack1HitBox;
    public GameObject MenuEnd;

    private Material matBlink;
    private Material matDefault;
    private SpriteRenderer SpriteRenderer;


    void Start()
    {
        Anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        attack1HitBox.SetActive(false);
        SpriteRenderer = GetComponent<SpriteRenderer>();
        matBlink = Resources.Load("EnemyBlink", typeof(Material)) as Material;
        matDefault = SpriteRenderer.material;
        Physics2D.queriesStartInColliders = false;

        //Time.timeScale = 0.3f;
        Physics2D.IgnoreLayerCollision(10, 10);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("DamageKnifeEnemySkeleton"))
        {
            //SpriteRenderer.material = matBlink;

            if(!DebugForCheats.isGodMod)
                HealthPlayer -= UnityEngine.Random.Range(5, 15);

            if (HealthPlayer > 0)
            {
                Invoke("ResetMatedial", 0.2f);
            }
        }
    }

    void ResetMatedial()
    {
        SpriteRenderer.material = matDefault;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (GameScript.isGame)
        {
            IsGrounded = Physics2D.OverlapCircle(GroundCheck.position, CheckRadious, WhatIsGround);

            //if (!IsGrounded && !IsOverEnemy)
            //{
            //    rb.velocity = new Vector2(0.0f, rb.velocity.y);
            //    RandomEnemyOver = 0;
            //}
            //IsOverEnemy = Physics2D.OverlapCircle(GroundCheck.position, CheckRadious, WhatIsEnemy);

            //if(IsOverEnemy && (AnimatorPlayer.IsAiring || Anim.GetCurrentAnimatorStateInfo(0).IsName("IdleDagger") || Anim.GetCurrentAnimatorStateInfo(0).IsName("IdleKatana")))
            //{
            //    if(RandomEnemyOver == 0)
            //        RandomEnemyOver = UnityEngine.Random.Range(1, 3);

            //    if (RandomEnemyOver == 1)
            //        rb.velocity = Vector2.right * JumpForce;
            //    else
            //        rb.velocity = Vector2.left * JumpForce;
            //}

            if ((Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.RightArrow))
                || (Input.GetKey(KeyCode.A)) && Input.GetKey(KeyCode.D)
                || (Input.GetKey(KeyCode.A)) && Input.GetKey(KeyCode.RightArrow)
                || (Input.GetKey(KeyCode.D)) && Input.GetKey(KeyCode.LeftArrow))
            {
                b_Left = false;
                b_Right = false;
            }
            else if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
            {
                b_Left = true;
                b_Right = false;
            }
            else if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
            {
                b_Left = false;
                b_Right = true;
            }
            else
            {
                b_Left = false;
                b_Right = false;
            }

            //
            //
            /*КОММЕНТАРИЙ: Ниже перечислен первый способ передвижения за счет velocity с помощью которого мы направляем игрока
            * TimerRun, Anim.speed время и скорость воспроизведения анимации */

            //    moveX = Input.GetAxis("Horizontal");
            //    rb.velocity = new Vector2(TimerRun > 0 ? moveX * speed * TimerRun : moveX * speed, rb.velocity.y);

            //    if (rb.velocity.x != 0)
            //    {
            //        if (TimerRun < 1)
            //            TimerRun += Time.deltaTime;
            //        else
            //            TimerRun = 1;
            //    }
            //    else
            //        TimerRun = 0;

            //    //Debug.Log("Speed: " + rb.velocity.x);
            //    //Debug.Log("TD: " + Time.deltaTime);
            //if (Anim.GetBool("IsRunning"))
            //{
            //    Anim.speed = rb.velocity.x > 0 ? rb.velocity.x / ConstNormalAnimationSpeed : (rb.velocity.x * -1) / ConstNormalAnimationSpeed;
            //}
            //else
            //    Anim.speed = 1f;

            //Debug.Log("Animation speed " + AnimRun.speed);
            //
            //

            /*КОММЕНТАРИЙ: Ниже перечислен второй способ передвижения за счет transform.Translate */
            if (Anim.GetBool("IsRunning"))
            {
                float movengX = Input.GetAxis("Horizontal");

                moveX = movengX == 0 ? moveX : movengX;

                Vector2 movement = new Vector2(moveX, 0.0f);

                movement = movement.normalized * speed * Time.fixedDeltaTime;

                //transform.Translate(movement * speed * Time.fixedDeltaTime);
                rb.MovePosition(rb.position + movement);
            }

            if ((!facingRight && moveX > 0) || (facingRight && moveX < 0))
                Flip();
        }
    }

    private void Update()
    {
        if (GameScript.isGame)
        {
            //HealthBar.fillAmount = FillHealth;

            if (HealthPlayer <= 0)
            {
                Invoke("GameOver", 1.0f);
            }

            if (Input.GetKeyDown(KeyCode.Space) && IsGrounded && !AnimatorPlayer.TackDamagePain)
            {
                Invoke("Jump", 0.5f);
            }

            if(Input.GetKeyDown(KeyCode.X) && (Anim.GetCurrentAnimatorStateInfo(0).IsName("IdleDagger") || Anim.GetCurrentAnimatorStateInfo(0).IsName("IdleKatana")) && !AnimatorPlayer.TackDamagePain)
            {
                //Debug.Log("OK!");

                TypeWeapon = !TypeWeapon;
                AnimatorPlayer.IndexSeriesAttack = 0;
            }
        }
    }

    void GameOver()
    {
        GameScript.isGame = false;
        MenuEnd.SetActive(true);
        //DataBaseScript.SaveDB();
    }

    void Jump()
    {
        if (IsGrounded)
        {
            rb.velocity = Vector2.up * JumpForce;

            //if(facingRight)
            //    rb.velocity = Vector2.right * JumpForce;
            //else
            //    rb.velocity = Vector2.left * JumpForce;

            PlayerSound.IsSoundJump = true;
            rb.velocity = new Vector2(transform.localScale.x * 40.0f, rb.velocity.y);
        }
    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 Scaler = transform.localScale;
        Scaler.x *= -1;
        transform.localScale = Scaler;
    }
}