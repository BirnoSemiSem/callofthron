﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPlayer : MonoBehaviour
{
    public float dumping = 1.5f; // сглаживание камеры. скольжение
    public Vector2 offset = new Vector2(2f, 10f); //смещение камеры относительно от персонажа. x y
    public bool isLeft; // смещение камеры. влево в вправо
    private Transform player; // положение персонажа
    private int lastx; // куда последний раз посмотрел персонаж

    [SerializeField]
    float limitleft;
    [SerializeField]
    float limitright;
    [SerializeField]
    float limitbottom;
    [SerializeField]
    float limitupper;


    void Start()
    {
        offset = new Vector2(Mathf.Abs(offset.x), offset.y); // вычесление смещения камеры
        FindPlayer(isLeft);
    }

    public void FindPlayer(bool playerIsLeft)
    {
        player = GameObject.FindGameObjectWithTag("Player").transform; // объект с тегом Player
        lastx = Mathf.RoundToInt(player.position.x);

        if (playerIsLeft)
            transform.position = new Vector3(player.position.x - offset.x, player.position.y - offset.y, transform.position.z);
        else
            transform.position = new Vector3(player.position.x + offset.x, player.position.y + offset.y, transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        if(player)
        {
            int currentx = Mathf.RoundToInt(player.position.x); // положение камеры относительно персонажа
            if (currentx > lastx)
                isLeft = false; // не смотрим на лево
            else if(currentx < lastx)
                isLeft = true; // смотрим на лево

            lastx = Mathf.RoundToInt(player.position.x);

            Vector3 target;
            if(isLeft)
            {
                target = new Vector3(player.position.x - offset.x, player.position.y + offset.y, transform.position.z);
            }
            else
            {
                target = new Vector3(player.position.x + offset.x, player.position.y + offset.y, transform.position.z);
            }

            Vector3 currentPosition = Vector3.Lerp(transform.position, target, dumping * Time.deltaTime);
            transform.position = currentPosition;
        }

        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, limitleft, limitright),
            Mathf.Clamp(transform.position.y, limitbottom, limitupper),
            transform.position.z);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(new Vector2(limitleft, limitupper), new Vector2(limitright, limitupper));
        Gizmos.DrawLine(new Vector2(limitleft, limitupper), new Vector2(limitleft, limitbottom));
        Gizmos.DrawLine(new Vector2(limitleft, limitbottom), new Vector2(limitright, limitbottom));
        Gizmos.DrawLine(new Vector2(limitright, limitupper), new Vector2(limitright, limitbottom));
    }
}
